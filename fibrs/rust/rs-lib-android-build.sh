# ENV
AARCH64_LINKER=$HOME/Library/Android/sdk/ndk/21.3.6528147/toolchains/llvm/prebuilt/darwin-x86_64/bin/aarch64-linux-android26-clang
ARMV7_LINKER=$HOME/Library/Android/sdk/ndk/21.3.6528147/toolchains/llvm/prebuilt/darwin-x86_64/bin/armv7a-linux-androideabi26-clang
I686_LINKER=$HOME/Library/Android/sdk/ndk/21.3.6528147/toolchains/llvm/prebuilt/darwin-x86_64/bin/i686-linux-android26-clang

# Build
CARGO_TARGET_AARCH64_LINUX_ANDROID_LINKER=$AARCH64_LINKER cargo build --target aarch64-linux-android --release
CARGO_TARGET_ARMV7_LINUX_ANDROIDEABI_LINKER=$ARMV7_LINKER cargo build --target armv7-linux-androideabi --release
CARGO_TARGET_I686_LINUX_ANDROID_LINKER=$I686_LINKER cargo build --target i686-linux-android --release