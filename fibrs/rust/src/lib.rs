#[no_mangle]
pub extern "C" fn fib(n: u32) -> u32 {
    return match n {
        0 => 0,
        1 => 1,
        _ => fib(n - 1) + fib(n - 2)
    }
}

#[cfg(test)]
mod libtests {
    use crate::fib;

    #[test]
    fn fibtest8() {
        assert_eq!(8, fib(6));
    }

    #[test]
    fn fibtest1(){
        assert_eq!(1, fib(1));
    }

    #[test]
    fn fibtest0(){
        assert_eq!(0, fib(0));
    }
}
